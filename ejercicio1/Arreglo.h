#include <iostream>

using namespace std;

#ifndef ARREGLO_H
#define ARREGLO_H

class Arreglo {
	
	private:
		int N;
		int *Numero = NULL;
	
	public:
		Arreglo(int N);
		int get_n();
		void set_n(int N);
		void Llenar_arreglo();
		void suma_de_cuadrados();


};
#endif
