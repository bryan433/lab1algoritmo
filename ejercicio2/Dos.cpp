#include <iostream>
#include <ctype.h>
#include "Dos.h"

using namespace std;


Dos::Dos (int N){
	this->N = N;
	this->Palabras = new string [this->N];
}
	

int Dos::get_n (){
	return this->N;
}


void Dos::set_n (int N){
	this-> N = N; 
}

void Dos::Llenar_arreglo (){
	string Palabras_ingresados;
	
	for (int i=0; i<N; i++){
		cout << "ingrese una Palabra para el arreglo, que contenga letras en mayuscula y minuscula: " << endl;
		cin >> Palabras_ingresados;
		Palabras[i] = Palabras_ingresados;
	}
}


void Dos::imprimir_arreglo(){
	for (int i=0; i<N; i++){
		int minuscula = 0;
		int mayuscula = 0;
		for (int j=0; j < Palabras[i].size(); j++){
			if (islower(Palabras[i][j])){
				minuscula = minuscula + 1;
			}
			else{
				mayuscula = mayuscula + 1;	
			}	
		}
		cout << "  " << endl;
		cout << "La palabra "<< Palabras[i] << " contiene " << minuscula << " minuscula/s y tiene " << mayuscula << " mayuscula/s" << endl;
		cout << "  " << endl;
	}

}
