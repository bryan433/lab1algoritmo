#include <iostream>

using namespace std;

#ifndef DOS_H
#define DOS_H

class Dos {
	
	private:
		int N;
		string *Palabras = NULL;
	
	public:
		Dos(int N);
		int get_n();
		void set_n(int N);
		void Llenar_arreglo();
		void imprimir_arreglo();
		//~ void suma_de_cuadrados();


};
#endif
